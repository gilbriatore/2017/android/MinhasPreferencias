package br.edu.up.minhaspreferencias;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

  private final static String ARQUIVO = "MinhasPrefs";
  private final static String COR = "cor";

  private Cor corAtual;
  private LinearLayout layout;
  private TextView textView;
  private Map<Integer, Cor> cores = new HashMap<>();
  {
    cores.put(0, new Cor(0, Color.BLACK, Color.WHITE));
    cores.put(1, new Cor(1, Color.WHITE, Color.BLACK));
    cores.put(2, new Cor(2, Color.WHITE, Color.BLUE));
    cores.put(3, new Cor(3, Color.BLACK, Color.YELLOW));
    cores.put(4, new Cor(4, Color.BLACK, Color.GREEN));
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    layout = (LinearLayout) findViewById(R.id.activity_main);
    textView = (TextView) findViewById(R.id.textView);

    SharedPreferences sp = getSharedPreferences(ARQUIVO, 0);
    int id = sp.getInt(COR, 0);
    corAtual = cores.get(id);
    layout.setBackgroundColor(corAtual.fundo);
    textView.setTextColor(corAtual.frente);
  }

  public void onClickAlterar(View v) {
    String tag = v.getTag().toString();
    int chave = Integer.parseInt(tag);
    corAtual = cores.get(chave);
    layout.setBackgroundColor(corAtual.fundo);
    textView.setTextColor(corAtual.frente);
  }

  @Override
  protected void onStop() {
    super.onStop();
    SharedPreferences sp = getSharedPreferences(ARQUIVO, 0);
    SharedPreferences.Editor editor = sp.edit();
    editor.putInt(COR, corAtual.id);
    editor.commit();
  }

  private class Cor {

    Integer id;
    Integer frente;
    Integer fundo;

    public Cor(Integer id, Integer frente, Integer fundo) {
      this.id = id;
      this.frente = frente;
      this.fundo = fundo;
    }
  }
}